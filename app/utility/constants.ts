export const Roles = {
    'ADMIN': '62cd75655853a5d2c643dbaa',
    'DOCTOR': '62cd75655853a5d2c643dbab',
    'NURSE': '62cd75655853a5d2c643dbac',
};

export const RequestStatus = {
    'PENDING': '62cd75655853a5d2c643dbaf',
    'REJECTED': '62cd75655853a5d2c643dbb0',
    'APPROVED': '62cd75655853a5d2c643dbb1'
};