export const roles = [
    { title: 'Admin' },
    { title: 'Doctor' },
    { title: 'Nurse' },
];

export const status = [
    { title: 'Pending', type: 'Change Request' },
    { title: 'Rejected', type: 'Change Request' },
    { title: 'Approved', type: 'Change Request' },
];