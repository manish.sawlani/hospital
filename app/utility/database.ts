import { hash } from "bcryptjs";
import { Model } from "mongoose";

import RoleModel from "../modules/role/role.schema";
import UserModel from '../modules/user/user.schema';
import StatusModel from '../modules/status/status.schema';
import { IUser } from "../modules/user/user.types";
import { Roles } from "./constants";
import { roles, status } from "./data";

const modelData = [
    { model: RoleModel, data: roles },
    { model: StatusModel, data: status }
];

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const populate = async (model: Model<any>, data: object) => {
    const result = await model.find();
    if (result.length) return;
    model.insertMany(data);
};

const createAdmin = async () => {
    const admins = await UserModel.find({ role: Roles.ADMIN });
    if (admins.length) return;
    const hashedPassword = await hash('123456', 12);
    const admin: IUser = {
        email: 'manish.sawlani@coditas.com',
        name: 'Manish Sawlani',
        password: hashedPassword,
        role: Roles.ADMIN
    };
    await UserModel.create(admin);
    console.log('Admin Created');
};

const initDB = async () => {
    for await (const model of modelData) {
        await populate(model.model, model.data); // Uncomment after adding data in data.js file
    }
    await createAdmin(); // Uncomment after updating constants.js file
};

export default initDB;