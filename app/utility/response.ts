/* eslint-disable @typescript-eslint/no-explicit-any */
export class ResponseHandler {
    constructor(
        public data: any = null,
        public error: any = null,
    ) { }
}