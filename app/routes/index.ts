import fs from "fs";
import path from "path";

import { Application, json, NextFunction, Request, Response } from "express";
import helmet from "helmet";
import cors from "cors";
import morgan from "morgan";

import { authorize } from "../utility/authorize";
import { ResponseHandler } from "../utility/response";

import { excludedPaths, routes } from "./routes.data";

export const registerRoutes = (app: Application) => {
    app.use(json());
    app.use(cors());
    app.use(helmet());
    app.use(morgan('common', {
        stream: fs.createWriteStream(path.join(process.cwd(), 'access.log'), { flags: 'a' })
    }));

    app.use(authorize(excludedPaths));

    for (const route of routes) {
        app.use(route.path, route.router);
    }

    // 404
    app.use((
        req: Request,
        res: Response,
    ) => {
        res.status(404).send(new ResponseHandler(null, { message: 'Url doesn\'t exists' }));
    });

    // Error
    app.use((
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        error: any,
        req: Request,
        res: Response,
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        next: NextFunction
    ) => {
        // console.log(error);
        res.status(error.statusCode || 500).send(new ResponseHandler(null, error));
    });
}