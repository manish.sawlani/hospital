import { IExclude, Route } from "./routes.types";

import UserRouter from "../modules/user/user.routes";
import MessageRouter from "../modules/message/message.routes";
import ChangeRequestRouter from "../modules/change-requests/change-request.routes";
import StatusRouter from "../modules/status/status.routes";
import ReminderRouter from "../modules/reminder/reminder.routes";

export const routes: Route[] = [
    new Route('/user', UserRouter),
    new Route('/change-request', ChangeRequestRouter),
    new Route('/message', MessageRouter),
    new Route('/status', StatusRouter),
    new Route('/reminder', ReminderRouter)
];

export const excludedPaths: IExclude[] = [
    { path: '/user/login', method: 'POST' },
    { path: '/user/reset-password', method: 'POST' },
    { path: '/user/new-password', method: 'POST' },
];