import { Request, Response, NextFunction, Router } from "express";
import { permit } from "../../utility/authorize";
import { Roles } from "../../utility/constants";
import { ResponseHandler } from "../../utility/response";
import changeRequestService from "./change-request.service";
import { IRequestFilters } from "./change-request.types";

const router = Router();

// Add Request
router.post('/',
    permit([Roles.NURSE, Roles.DOCTOR]),
    async (
        req: Request,
        res: Response,
        next: NextFunction
    ) => {
        try {
            const { for: requestFor, replacement, reason } = req.body;
            const result = await changeRequestService.addRequest({ from: res.locals.user._id, for: requestFor, replacement, reason });
            res.send(new ResponseHandler(result));
        } catch (error) {
            next(error);
        }
    }
);

// View Requests
router.get('/',
    permit([Roles.DOCTOR, Roles.NURSE, Roles.ADMIN]),
    async (
        req: Request,
        res: Response,
        next: NextFunction
    ) => {
        try {
            const result = await changeRequestService.viewRequests(req.query as IRequestFilters, res.locals.user._id, res.locals.user.role);
            res.send(new ResponseHandler(result));
        } catch (error) {
            next(error);
        }
    }
);

// Update Request Status
router.patch('/:id',
    permit([Roles.DOCTOR, Roles.NURSE, Roles.ADMIN]),
    async (
        req: Request,
        res: Response,
        next: NextFunction
    ) => {
        try {
            const result = await changeRequestService.updateRequestStatus(req.params.id, req.body.status);
            res.send(new ResponseHandler(result));
        } catch (error) {
            next(error);
        }
    }
);

export default router;