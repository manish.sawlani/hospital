import { Schema, SchemaTypes, model, Document } from "mongoose";
import { RequestStatus } from "../../utility/constants";
import { IChangeRequest } from "./change-request.types";

class ChangeRequestSchema extends Schema {
    constructor() {
        super({
            from: {
                type: SchemaTypes.ObjectId,
                ref: 'User',
                required: true
            },
            for: {
                type: SchemaTypes.ObjectId,
                ref: 'User',
                required: true
            },
            replacement: {
                type: SchemaTypes.ObjectId,
                ref: 'User',
                required: true
            },
            reason: {
                type: String,
                required: true
            },
            status: {
                type: SchemaTypes.ObjectId,
                ref: 'Status',
                default: RequestStatus.PENDING
            },
        }, { timestamps: true });
    }
}

type ChangeRequestDocument = Document & IChangeRequest;
const ChangeRequestModel = model<ChangeRequestDocument>('ChangeRequest', new ChangeRequestSchema());
export default ChangeRequestModel;