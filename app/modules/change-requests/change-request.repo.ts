import { Types } from "mongoose";
import { Roles } from "../../utility/constants";
import ChangeRequestModel from "./change-request.schema";
import { IChangeRequest, IRequestFilters } from "./change-request.types";

const addRequest = (request: IChangeRequest) => ChangeRequestModel.create(request);

const viewRequests = (requestFilter: IRequestFilters, accessor: string, accessorRole: string) => {
    const { page, itemsPerPage, status } = requestFilter;

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const agg: any[] = [];
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const matchQueries: any[] = [];

    const match = {
        $match: {
            $and: matchQueries
        }
    };

    if (status) matchQueries.push({ status: new Types.ObjectId(status) });

    if (accessorRole === Roles.DOCTOR || accessorRole === Roles.NURSE) matchQueries.push({ from: new Types.ObjectId(accessor) });

    if (matchQueries.length) agg.push(match);

    if (page && itemsPerPage) {
        agg.push({ $skip: (+page - 1) * +itemsPerPage });
        agg.push({ $limit: +itemsPerPage });
    }

    return ChangeRequestModel.aggregate([
        ...agg,
        {
            $lookup: {
                from: 'users',
                localField: 'for',
                foreignField: '_id',
                as: 'for'
            }
        },
        {
            $lookup: {
                from: 'users',
                localField: 'from',
                foreignField: '_id',
                as: 'from'
            }
        },
        {
            $lookup: {
                from: 'users',
                localField: 'replacement',
                foreignField: '_id',
                as: 'replacement'
            }
        },
        {
            $lookup: {
                from: 'status',
                localField: 'status',
                foreignField: '_id',
                as: 'status'
            }
        }
    ]);
};

const updateRequestStatus = (id: string, status: string) => ChangeRequestModel.findByIdAndUpdate(id, { $set: { status: status } });

const findById = (id: string) => ChangeRequestModel.findById(id);

export default {
    addRequest,
    viewRequests,
    updateRequestStatus,
    findById
};