import { RequestStatus, Roles } from "../../utility/constants";
import userService from "../user/user.service";
import { IUser } from "../user/user.types";
import changeRequestRepo from "./change-request.repo";
import { IChangeRequest, IRequestFilters } from "./change-request.types";

const addRequest = (request: IChangeRequest) => changeRequestRepo.addRequest(request);

const viewRequests = (requestFilter: IRequestFilters, accessor: string, accessorRole: string) => changeRequestRepo.viewRequests(requestFilter, accessor, accessorRole);

const updateRequestStatus = async (id: string, status: string) => {
    const request = await changeRequestRepo.findById(id);
    if (!request) throw { statusCode: 400, message: 'Invalid request id' };
    if (request.status?.toString() === RequestStatus.APPROVED) throw { statusCode: 400, message: 'Request already approved' };

    const requestFrom = await userService.getOne(request.from);
    const requestFor = await userService.getOne(request.for);
    const replacement = await userService.getOne(request.replacement);

    if (!requestFor || !requestFrom || !replacement) throw { statusCode: 500, message: 'Something went wrong!' };

    if (requestFrom.role.toString() === Roles.NURSE && status === RequestStatus.APPROVED) { // If the request is from a nurse
        // Update assignedDoctor to the new replacement doctor
        requestFrom.assignedDoctor = replacement.id;
        // From the previous doctor's nurses, remove the requesting nurse
        requestFor.nurses = (requestFor.nurses as IUser[])?.filter((nurse: IUser) => nurse._id?.toString() !== requestFrom._id.toString());
        // Add this nurse in replacement doctor's list
        replacement.nurses?.push(requestFrom._id);
    } else if (requestFrom.role.toString() === Roles.DOCTOR && status === RequestStatus.APPROVED) { // If the request is from a doctor
        // Remove the nurse from doctor's nurse list
        requestFrom.nurses = (requestFrom.nurses as IUser[])?.filter((nurse: IUser) => nurse._id?.toString() !== requestFor._id.toString());
        // Add replacement nurse in nurse list
        requestFrom.nurses.push(replacement._id);
        // Remove assigned doctor from the nurse's document
        requestFor.assignedDoctor = null;
        // Add doctor to replacement's assigned doctor
        replacement.assignedDoctor = requestFrom._id;
    }

    await requestFrom.save();
    await requestFor.save();
    await replacement.save();
    return changeRequestRepo.updateRequestStatus(id, status);
}

export default {
    addRequest,
    viewRequests,
    updateRequestStatus
};