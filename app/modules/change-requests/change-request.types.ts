export interface IChangeRequest {
    _id?: string,
    from: string,
    for: string,
    replacement: string,
    reason: string,
    status?: string,
}

export interface IRequestFilters {
    page?: number,
    itemsPerPage?: number,
    status?: string
}