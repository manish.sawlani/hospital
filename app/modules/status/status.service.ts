import statusRepo from "./status.repo";

const getAll = () => statusRepo.getAll();

export default {
    getAll
};