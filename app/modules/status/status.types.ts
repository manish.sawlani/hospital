export interface IStatus {
    _id?: string,
    title: string,
    type: string
}