import StatusModel from "./status.schema";

const getAll = () => StatusModel.find();

export default {
    getAll
};