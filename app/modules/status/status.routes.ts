import { Request, Response, NextFunction, Router } from "express";
import statusService from "./status.service";

import { permit } from "../../utility/authorize";
import { Roles } from "../../utility/constants";
import { ResponseHandler } from "../../utility/response";

const router = Router();

// All Status
router.get('/',
    permit([Roles.ADMIN, Roles.DOCTOR, Roles.NURSE]),
    async (
        req: Request,
        res: Response,
        next: NextFunction
    ) => {
        try {
            const result = await statusService.getAll();
            res.send(new ResponseHandler(result));
        } catch (error) {
            next(error);
        }
    }
);

export default router;