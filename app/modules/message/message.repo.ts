import { Types } from "mongoose";
import { Roles } from "../../utility/constants";
import MessageModel from "./message.schema";
import { IMessage, IMessageFilters } from "./message.types";

const addMessage = (message: IMessage) => MessageModel.create(message);

const viewMessages = (accessor: string, accessorRole: string, messageFilters: IMessageFilters) => {

    const { page, itemsPerPage } = messageFilters;

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const agg: any[] = [];
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const matchQueries: any[] = [];

    const match = {
        $match: {
            $and: matchQueries
        }
    };

    if (accessorRole === Roles.DOCTOR)
        matchQueries.push({ to: new Types.ObjectId(accessor) });
    else if (accessorRole === Roles.NURSE)
        matchQueries.push({ from: new Types.ObjectId(accessor) });

    if (matchQueries.length) agg.push(match);

    if (page && itemsPerPage) {
        agg.push({ $skip: (+page - 1) * +itemsPerPage });
        agg.push({ $limit: +itemsPerPage });
    }

    return MessageModel.aggregate([
        ...agg,
        {
            $lookup: {
                from: 'users',
                localField: 'from',
                foreignField: '_id',
                as: 'from'
            }
        },
        // {
        //     $lookup: {
        //         from: 'users',
        //         localField: 'to',
        //         foreignField: '_id',
        //         as: 'to'
        //     }
        // }
    ]);
}

export default {
    addMessage,
    viewMessages
};