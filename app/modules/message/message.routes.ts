import { NextFunction, Request, Response, Router } from "express";

import messageService from "./message.service";
import { IMessageFilters } from "./message.types";
import { CreateMessageValidator } from "./message.validations";

import { permit } from "../../utility/authorize";
import { Roles } from "../../utility/constants";
import { ResponseHandler } from "../../utility/response";

const router = Router();

// View all message
router.get('/',
    permit([Roles.DOCTOR, Roles.NURSE]),
    async (
        req: Request,
        res: Response,
        next: NextFunction
    ) => {
        try {
            const result = await messageService.viewMessages(res.locals.user._id, res.locals.user.role, req.query as IMessageFilters);
            res.send(new ResponseHandler(result));
        } catch (error) {
            next(error);
        }
    }
);

// Post a message
router.post('/',
    CreateMessageValidator,
    permit([Roles.NURSE]),
    async (
        req: Request,
        res: Response,
        next: NextFunction
    ) => {
        try {
            const { to, text } = req.body;
            const result = await messageService.addMessage({ from: res.locals.user._id, to, text });
            res.send(new ResponseHandler(result));
        } catch (error) {
            next(error);
        }
    }
);

export default router;