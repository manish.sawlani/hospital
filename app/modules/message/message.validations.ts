import { body } from "express-validator";
import validate from "../../utility/validate";

export const CreateMessageValidator = [
    body("to").trim().notEmpty().isString().withMessage("Please select the receipent"),
    body("text").trim().notEmpty().isString().withMessage("Please enter a message"),
    validate
];