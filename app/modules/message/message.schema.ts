import { Schema, model, Document, SchemaTypes } from "mongoose";
import { IMessage } from "./message.types";

class MessageSchema extends Schema {
    constructor() {
        super({
            from: {
                type: SchemaTypes.ObjectId,
                ref: 'User',
                required: true
            },
            to: {
                type: SchemaTypes.ObjectId,
                ref: 'User',
                required: true
            },
            text: {
                type: String,
                required: true
            }
        }, { timestamps: true });
    }
}

type MessageDocument = Document & IMessage;
const MessageModel = model<MessageDocument>('Message', new MessageSchema());
export default MessageModel;