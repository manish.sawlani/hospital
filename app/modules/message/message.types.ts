export interface IMessage {
    _id?: string,
    from: string,
    to: string,
    text: string
}

export interface IMessageFilters {
    page?: number,
    itemsPerPage?: number
}