import messageRepo from "./message.repo";
import { IMessage, IMessageFilters } from "./message.types";

const addMessage = (message: IMessage) => messageRepo.addMessage(message);

const viewMessages = (accessor: string, accessorRole: string, filters: IMessageFilters) => messageRepo.viewMessages(accessor, accessorRole, filters);

export default {
    addMessage,
    viewMessages
};