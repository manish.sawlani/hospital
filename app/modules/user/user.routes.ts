import { Request, Response, NextFunction, Router } from "express";

import { CreateUserValidator, GetResetPassword, LoginValidator, ResetPasswordValidator } from "./user.validations";
import userService from "./user.service";

import { ResponseHandler } from "../../utility/response";
import { permit } from "../../utility/authorize";
import { Roles } from "../../utility/constants";
import { IFilters } from "./user.types";

const router = Router();

// User registration
router.post(
    "/register",
    CreateUserValidator,
    permit([Roles.ADMIN]),
    async (
        req: Request,
        res: Response,
        next: NextFunction
    ) => {
        try {
            const result = await userService.register(req.body);
            res.send(new ResponseHandler(result));
        } catch (error) {
            next(error);
        }
    }
);

// User login
router.post(
    "/login",
    LoginValidator,
    async (
        req: Request,
        res: Response,
        next: NextFunction
    ) => {
        try {
            const result = await userService.authenticate(req.body);
            res.send(new ResponseHandler(result));
        } catch (error) {
            next(error);
        }
    }
);

// Verify Token
router.post(
    "/verify-token",
    async (
        req: Request,
        res: Response,
        next: NextFunction
    ) => {
        try {
            res.send(new ResponseHandler({ _id: res.locals.user._id, role: res.locals.user.role }));
        } catch (error) {
            next(error);
        }
    }
);

// Post reset password
router.post(
    "/reset-password",
    GetResetPassword,
    async (
        req: Request,
        res: Response,
        next: NextFunction
    ) => {
        try {
            const result = await userService.getResetPassword(req.body.email);
            res.send(new ResponseHandler(result));
        } catch (error) {
            next(error);
        }
    }
);

// Post new password
router.post(
    "/new-password",
    ResetPasswordValidator,
    async (
        req: Request,
        res: Response,
        next: NextFunction
    ) => {
        try {
            const result = await userService.postResetPassword(
                req.body.resetToken,
                req.body.password
            );
            res.send(new ResponseHandler(result));
        } catch (error) {
            next(error);
        }
    }
);

// Get All User Data
router.get('/',
    permit([Roles.ADMIN, Roles.DOCTOR, Roles.NURSE]),
    async (
        req: Request,
        res: Response,
        next: NextFunction
    ) => {
        try {
            const result = await userService.getAll(req.query as IFilters);
            res.send(new ResponseHandler(result));
        } catch (error) {
            next(error);
        }
    }
);

// Get User Profile
router.get('/profile',
    permit([Roles.ADMIN, Roles.DOCTOR, Roles.NURSE]),
    async (
        req: Request,
        res: Response,
        next: NextFunction
    ) => {
        try {
            const result = await userService.getOne(res.locals.user._id);
            res.send(new ResponseHandler(result));
        } catch (error) {
            next(error);
        }
    }
);

// Edit User
router.patch('/:id',
    permit([Roles.ADMIN]),
    async (
        req: Request,
        res: Response,
        next: NextFunction
    ) => {
        try {
            const result = await userService.editUser(req.params.id, req.body);
            res.send(new ResponseHandler(result));
        } catch (error) {
            next(error);
        }
    }
);

// Delete User
router.delete('/:id',
    permit([Roles.ADMIN]),
    async (
        req: Request,
        res: Response,
        next: NextFunction
    ) => {
        try {
            const result = await userService.deleteUser(req.params.id, res.locals.user.role);
            res.send(new ResponseHandler(result));
        } catch (error) {
            next(error);
        }
    }
);

export default router;