import { body } from "express-validator";
import { Roles } from "../../utility/constants";
import validate from "../../utility/validate";

export const CreateUserValidator = [
    body("name").trim().notEmpty().isString().withMessage("Please provide a valid name"),
    body("email").trim().notEmpty().isEmail().withMessage("Please provide a valid email"),
    body("role").custom((value, { req }) => {
        if (value && !Object.values(Roles).includes(value)) throw 'Please provide a valid role';
        if (value && value === Roles.DOCTOR && !req.body.speciality) throw `Please enter speciality of ${req.body.name}`;
        return true;
    }),
    validate
];

export const LoginValidator = [
    body("email").trim().notEmpty().isEmail().withMessage("Please provide a valid email"),
    body("password").trim().notEmpty().isString().withMessage("Please provide a valid password").isLength({ min: 6 }).withMessage("Password must be atleast 6 characters long"),
    validate
];

export const GetResetPassword = [
    body("email").trim().notEmpty().isEmail().withMessage("Please enter a valid email"),
    validate
];

export const ResetPasswordValidator = [
    body("resetToken").trim().notEmpty().isString().withMessage("Invalid reset token"),
    body("password").trim().notEmpty().isString().withMessage("Please provide a valid password").isLength({ min: 6 }).withMessage("Password must be atleast 6 characters long"),
    body("confirmPassword").trim().notEmpty().isString().custom((value, { req }) => {
        if (value !== req.body.password) throw 'Passwords do not match';
        return true;
    }),
    validate
];