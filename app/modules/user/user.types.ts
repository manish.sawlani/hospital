export interface IUser {
    _id?: string
    name: string,
    email: string,
    password: string,
    role: string,
    assignedDoctor?: string | null | IUser,
    speciality?: string,
    nurses?: string[] | IUser[],
    resetToken?: string | null,
    resetTokenExpiry?: number | null
}

export interface IFilters {
    page?: number,
    itemsPerPage?: number,
    role?: string,
    occupied?: string,
    assignedDoctor?: string,
    nurse?: string
}

export interface ICredentials {
    email: string,
    password: string,
}