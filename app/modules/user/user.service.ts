import { compare, hash } from "bcryptjs";
import { randomBytes } from "crypto";
import { sign } from "jsonwebtoken";
import sgMail from "@sendgrid/mail";
sgMail.setApiKey(process.env.SENDGRID_API || '');

import userRepo from "./user.repo";
import { ICredentials, IFilters, IUser } from "./user.types";
import { Roles } from "../../utility/constants";

const register = async (user: IUser) => {
    const { name, email, role, assignedDoctor, nurses } = user;
    const existingUser = await userRepo.findByEmail(email);
    if (existingUser) throw { statusCode: 421, message: 'User already exists' };

    const hashedPassword = await hash(`${name}@123`, 12);
    const userData = {
        ...user,
        password: hashedPassword,
        resetToken: randomBytes(32).toString('hex'),
        resetTokenExpiry: Date.now() + (1000 * 60 * 60 * 24)
    };

    const result = await userRepo.create(userData);

    if (role === Roles.NURSE && assignedDoctor) {
        const doctor = await userRepo.findById(assignedDoctor as string);
        if (!doctor) throw { statusCode: 400, message: "Invalid doctor id" };

        doctor.nurses?.push(result?._id);
        await doctor.save();
    } else if (role === Roles.DOCTOR) {
        for await (const nurseId of nurses as string[]) {
            const nurse = await userRepo.findById(nurseId);
            if (!nurse) throw { statusCode: 400, message: `Invalid nurse id ${nurse}` };

            nurse.assignedDoctor = result?._id;
            await nurse.save();
        }
    }

    if (!result._id) throw { statusCode: 500, message: 'Something went wrong!' };

    await sgMail.send({
        to: user.email,
        from: 'manish.sawlani@coditas.com',
        subject: 'Password Reset',
        html: `<html>
                <body>
                    <p>Your account has been created successfully.</p>
                    <p>Please <a href="http://localhost:4200/auth/reset-password/${result.resetToken}">click here</a> to create your password.</p>
                    <p>This link is valid only for 24 hours. After that, please contact your administrator.</p>
                </body>
            </html>`
    });
    return { _id: result?._id, message: "User registered successfully" };
};

const authenticate = async (credentials: ICredentials) => {
    const { email, password } = credentials;
    const user = await userRepo.findByEmail(email);
    if (!user) throw { statusCode: 421, message: 'User does not exists' };

    const doMatch = await compare(password, user.password);
    if (!doMatch) throw { statusCode: 421, message: 'Invalid password' };

    const tokenData = {
        _id: user._id,
        name: user.name,
        email: user.email,
        password: user.password,
        role: user.role
    };

    const token = sign(tokenData, process.env.JWT_SECRET || '', { expiresIn: '24h' });
    return { token, userId: user._id, role: user.role, name: user.name, tokenExpiry: Date.now() + (1000 * 60 * 60 * 24) };
};

const getResetPassword = async (email: string) => {
    const user = await userRepo.findByEmail(email);
    if (!user) throw { statusCode: 421, message: 'User does not exists' };
    if (
        user.resetToken &&
        user.resetTokenExpiry &&
        Date.now() <= user.resetTokenExpiry
    ) throw { statusCode: 400, message: 'An email with the reset link has already been sent to the registered email. Kindly check your inbox' };

    user.resetToken = randomBytes(32).toString('hex');
    user.resetTokenExpiry = Date.now() + (1000 * 60 * 60);

    const result = await user.save();
    if (result._id && result.resetToken) {
        await sgMail.send({
            to: user.email,
            from: 'manish.sawlani@coditas.com',
            subject: 'Password Reset',
            html: `<html>
                <body>
                    <p>Please <a href="http://localhost:4200/auth/reset-password/${result.resetToken}">click here</a> to reset your password.</p>
                </body>
            </html>`
        });
        return { _id: result._id, message: "Email sent!" };
    }
    throw { statusCode: 500, message: 'Something went wrong!' };
};

const postResetPassword = async (resetToken: string, password: string) => {
    const user = await userRepo.findByToken(resetToken);
    if (
        !user ||
        user.resetToken && (resetToken !== user.resetToken) ||
        user.resetTokenExpiry && (Date.now() >= user.resetTokenExpiry)
    ) throw { statusCode: 400, message: 'Reset token invalid' };

    user.password = await hash(password, 12);
    user.resetToken = null;
    user.resetTokenExpiry = null;

    const result = await user.save();
    if (result._id) {
        await sgMail.send({
            to: user.email,
            from: 'manish.sawlani@coditas.com',
            subject: 'Attention: Password Reset',
            text: `Your password has successfully been reset.`,
        });
        return { _id: result._id, message: "Password reset successful!" };
    }
    throw { statusCode: 500, message: 'Something went wrong!' };
};

const getAll = (filters: IFilters) => userRepo.findAll(filters);

const getOne = (id: string) => userRepo.findById(id);

const editUser = async (id: string, updatedUserData: IUser) => {
    const user = await userRepo.findById(id);
    if (!user) throw { statusCode: 400, message: 'Invalid user id' };

    if (user.role === Roles.NURSE && updatedUserData.assignedDoctor !== user.assignedDoctor) {
        // Fetch current doctor
        if (user.assignedDoctor) {
            const currentDoctor = await userRepo.findById(user.assignedDoctor as string);
            if (!currentDoctor) throw { statusCode: 400, message: 'Can not find previous doctor' };
            currentDoctor.nurses = (currentDoctor.nurses as IUser[])?.filter((nurse: IUser) => nurse._id?.toString() !== user._id.toString());
            await currentDoctor.save();
        }
        // Fetch replacement doctor
        const replacementDoctor = await userRepo.findById(updatedUserData.assignedDoctor as string);
        if (!replacementDoctor) throw { statusCode: 400, message: 'Invalid doctor id' };
        replacementDoctor.nurses?.push(user._id);
        await replacementDoctor.save();

    } else if (user.role === Roles.DOCTOR && (!(user.nurses as string[])?.every((nurse: any) => updatedUserData.nurses?.includes(nurse)) || user.nurses?.length !== updatedUserData.nurses?.length)) {

        // Find out new nurses
        const newNurses = (updatedUserData.nurses as string[])?.filter((nurse: any) => !user.nurses?.includes(nurse));
        // Update the new nurses assignedDoctor
        for await (const nurseId of newNurses as string[]) {
            const nurse = await userRepo.findById(nurseId);
            if (!nurse) throw { statusCode: 400, message: `Invalid nurse id ${nurse}` };

            nurse.assignedDoctor = user._id;
            await nurse.save();
        }

        // Find out old nurses
        const oldNurses = (user.nurses as string[])?.filter((nurse: any) => !updatedUserData.nurses?.includes(nurse));
        // Update old nurses assignedDoctor
        for await (const nurseId of oldNurses as string[]) {
            const nurse = await userRepo.findById(nurseId);
            if (!nurse) throw { statusCode: 400, message: `Invalid nurse id ${nurse}` };

            nurse.assignedDoctor = null;
            await nurse.save();
        }
    }

    return userRepo.updateOne(id, updatedUserData);
}

const deleteUser = async (id: string, role: string) => {
    if (role === Roles.NURSE) {
        const [doctor] = await userRepo.findAll({ nurse: id });
        await userRepo.removeNurse(doctor._id, id);
    } else if (role === Roles.DOCTOR) {
        const [nurse] = await userRepo.findAll({ assignedDoctor: id })
        await userRepo.removeDoctor(nurse._id);
    }
    return userRepo.deleteOne(id);
};

// To remove assignedDoctor from nurse's document
const removeDoctor = (nurseId: string) => userRepo.removeDoctor(nurseId);

// To pull out nurse for the given doctor id
const removeNurse = (doctorId: string, nurseId: string) => userRepo.removeNurse(doctorId, nurseId);

export default {
    register,
    authenticate,
    getResetPassword,
    postResetPassword,
    getAll,
    getOne,
    deleteUser,
    editUser,
    removeDoctor,
    removeNurse
};