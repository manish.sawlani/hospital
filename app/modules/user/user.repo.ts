import { Types } from "mongoose";
import { Roles } from "../../utility/constants";

import UserModel from "./user.schema";
import { IFilters, IUser } from "./user.types";

const create = (user: IUser) => UserModel.create(user);

const findByEmail = (email: string) => UserModel.findOne({ email: email });

const findById = (id: string) => UserModel.findById(id).populate("nurses").populate("assignedDoctor");

const findByToken = (token: string) => UserModel.findOne({ resetToken: token });

const findAll = (filters: IFilters) => {
    const { page, itemsPerPage, role, occupied, assignedDoctor, nurse } = filters;

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const agg: any[] = [];
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const matchQueries: any[] = [];

    const match = {
        $match: {
            $and: matchQueries
        }
    };

    if (role) matchQueries.push({ role: new Types.ObjectId(role) });

    if (role === Roles.NURSE && occupied && occupied === 'false') matchQueries.push({ assignedDoctor: null });
    else if (role === Roles.NURSE && occupied && occupied === 'true') matchQueries.push({ assignedDoctor: { $exists: true } });
    else if (role === Roles.NURSE && occupied) throw { statusCode: 400, message: 'Please enter true/false for "occupied" parameter' };

    if (assignedDoctor) matchQueries.push({ assignedDoctor: new Types.ObjectId(assignedDoctor) });

    if (nurse) matchQueries.push({ nurses: new Types.ObjectId(nurse) });

    if (matchQueries.length) agg.push(match);

    if (page && itemsPerPage) {
        agg.push({ $skip: (+page - 1) * +itemsPerPage });
        agg.push({ $limit: +itemsPerPage });
    }

    return UserModel.aggregate([
        ...agg,
        {
            $lookup: {
                from: 'users',
                localField: 'assignedDoctor',
                foreignField: '_id',
                as: 'assignedDoctor'
            }
        },
        {
            $lookup: {
                from: 'users',
                localField: 'nurses',
                foreignField: '_id',
                as: 'nurses'
            }
        }
    ]);

}

const removeNurse = (doctorId: string, nurseId: string) => UserModel.findByIdAndUpdate(doctorId, {
    $pull: {
        nurses: new Types.ObjectId(nurseId)
    }
});

const removeDoctor = (nurseId: string) => UserModel.findByIdAndUpdate(nurseId, {
    $set: {
        assignedDoctor: null
    }
});

const updateOne = (id: string, updatedUserData: IUser) => UserModel.findByIdAndUpdate(id, {
    $set: {
        ...updatedUserData
    }
});

const deleteOne = (id: string) => UserModel.findByIdAndDelete(id);

export default {
    create,
    findByEmail,
    findById,
    findByToken,
    findAll,
    removeDoctor,
    removeNurse,
    updateOne,
    deleteOne
};