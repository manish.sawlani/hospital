import { Schema, model, Document, SchemaTypes } from "mongoose";
import { Roles } from "../../utility/constants";
import { IUser } from "./user.types";

class UserSchema extends Schema {
    constructor() {
        super({
            name: {
                type: String,
                required: true
            },
            email: {
                type: String,
                required: true
            },
            password: {
                type: String,
                required: true
            },
            role: {
                type: SchemaTypes.ObjectId,
                required: true,
                default: Roles.ADMIN
            },
            assignedDoctor: {
                type: SchemaTypes.ObjectId,
                ref: 'User',
                default: null
            },
            speciality: {
                type: String,
                default: null
            },
            nurses: [{
                type: SchemaTypes.ObjectId,
                ref: 'User',
                default: null
            }],
            resetToken: {
                type: String,
                default: null
            },
            resetTokenExpiry: {
                type: Number,
                default: null
            },
        }, { timestamps: true });
    }
}

type UserDocument = Document & IUser;
const UserModel = model<UserDocument>('User', new UserSchema());
export default UserModel;