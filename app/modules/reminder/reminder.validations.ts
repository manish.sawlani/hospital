import { param } from "express-validator";
import validate from "../../utility/validate";

export const AddReminderValidator = [
    param("id").trim().notEmpty().withMessage("Please provide reminder id"),
    validate
];