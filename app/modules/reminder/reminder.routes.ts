import { NextFunction, Request, Response, Router } from "express";

import reminderService from "./reminder.service";
import { AddReminderValidator } from "./reminder.validations";

import { permit } from "../../utility/authorize";
import { Roles } from "../../utility/constants";
import { ResponseHandler } from "../../utility/response";

const router = Router();

router.post('/:id',
    AddReminderValidator,
    permit([Roles.NURSE, Roles.DOCTOR]),
    async (
        req: Request,
        res: Response,
        next: NextFunction
    ) => {
        try {
            const result = await reminderService.addReminder(req.params.id);
            res.send(new ResponseHandler(result));
        } catch (error) {
            next(error);
        }
    }
);

router.get('/',
    permit([Roles.ADMIN]),
    async (
        req: Request,
        res: Response,
        next: NextFunction
    ) => {
        try {
            const result = await reminderService.viewReminders(req.query);
            res.send(new ResponseHandler(result));
        } catch (error) {
            next(error);
        }
    }
);

router.delete('/:id',
    permit([Roles.ADMIN]),
    async (
        req: Request,
        res: Response,
        next: NextFunction
    ) => {
        try {
            const result = await reminderService.deleteReminder(req.params.id);
            res.send(new ResponseHandler(result));
        } catch (error) {
            next(error);
        }
    }
);

export default router;