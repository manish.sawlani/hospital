import ReminderModel from "./reminder.schema";
import {  IReminderFilters } from "./reminder.types";

const addReminder = (id: string) => ReminderModel.create({ request: id });

const viewReminders = (filters: IReminderFilters) => {
    const { page, itemsPerPage, deleted } = filters;

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const agg: any[] = [];
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const matchQueries: any[] = [];

    const match = {
        $match: {
            $and: matchQueries
        }
    };

    if (deleted === 'false') matchQueries.push({ deleted: false });
    else if (deleted === 'true') matchQueries.push({ deleted: true });
    else if(deleted) throw {statusCode: 400, messgae: 'Please enter true/false for "deleted" parameter' };

    if (matchQueries.length) agg.push(match);

    if (page && itemsPerPage) {
        agg.push({ $skip: (+page - 1) * +itemsPerPage });
        agg.push({ $limit: +itemsPerPage });
    }

    return ReminderModel.aggregate([
        ...agg,
        {
            $lookup: {
                from: 'changerequests',
                localField: 'request',
                foreignField: '_id',
                as: 'request'
            }
        },
        {
            $lookup: {
                from: 'users',
                localField: 'request.from',
                foreignField: '_id',
                as: 'request.from'
            }
        },
    ]);
};

const deleteReminder = (id: string) => ReminderModel.findByIdAndUpdate(id, {
    $set: {
        deleted: true
    }
});

export default {
    addReminder,
    viewReminders,
    deleteReminder
};