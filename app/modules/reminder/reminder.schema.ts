import { Schema, model, Document, SchemaTypes } from "mongoose";
import { IReminder } from "./reminder.types";

class ReminderSchema extends Schema {
    constructor() {
        super({
            request: {
                type: SchemaTypes.ObjectId,
                ref: 'ChangeRequest',
                required: true
            },
            deleted: {
                type: Boolean,
                default: false
            }
        }, { timestamps: true });
    }
}

type ReminderDocument = Document & IReminder;
const ReminderModel = model<ReminderDocument>('Reminder', new ReminderSchema());
export default ReminderModel;