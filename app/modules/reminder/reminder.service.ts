import reminderRepo from "./reminder.repo";
import { IReminderFilters } from "./reminder.types";

const addReminder = (id: string) => reminderRepo.addReminder(id);

const viewReminders = (filters: IReminderFilters) => reminderRepo.viewReminders(filters);

const deleteReminder = (id: string) => reminderRepo.deleteReminder(id);

export default {
    addReminder,
    viewReminders,
    deleteReminder
};