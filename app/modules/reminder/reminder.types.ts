import { IChangeRequest } from "../change-requests/change-request.types"

export interface IReminder {
    _id?: string,
    request: string | IChangeRequest,
    deleted?: boolean
}

export interface IReminderFilters {
    page?: number,
    itemsPerPage?: number,
    deleted?: string
}