import { connect } from "mongoose";
import initDB from "../utility/database";

export const connectToMongo = async () => {
    const MONGO_CONNECTION = process.env.MONGO_CONNECTION || '';
    await connect(MONGO_CONNECTION);
    console.log('Connected to MongoDB');
    await initDB();
};